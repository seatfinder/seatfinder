#FROM oracle/graalvm-ce:20.0.0-java8 as graalvm
# For JDK 11
FROM oracle/graalvm-ce:20.0.0-java11 as graalvm
RUN gu install native-image

COPY . /home/app/seatfinder
WORKDIR /home/app/seatfinder

RUN native-image --no-server -cp build/libs/seatfinder-*-all.jar

FROM frolvlad/alpine-glibc
RUN apk update && apk add libstdc++
EXPOSE 8080 5432
COPY --from=graalvm /home/app/seatfinder/seatfinder /app/seatfinder
ENTRYPOINT ["/app/seatfinder"]
