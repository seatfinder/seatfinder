package seatfinder.fetcher;

import io.micronaut.context.annotation.Requires;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.scheduling.annotation.Scheduled;
import seatfinder.model.measurement.DeltaPeople;
import seatfinder.model.sensor.Ase;
import seatfinder.service.client.AseClient;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

@Singleton
@Requires(property = "ase.fetch")
public class AseFetcher {

    @Inject
    AseClient client;

    @Inject
    @Client("https://zhaw.diamondreports.ch:8012/api/location/all")
    HttpClient ase;

    @Scheduled(fixedRate = "${ase.fetch.fixedRate:1m}")
    void fetchWlanMeasurement(){
        // fetching with query parameter ?date={date}&aggregate={(1|5|10|15|30|60)}
        System.out.println("ase fetching");
        // storing
        ArrayList<Ase> ases = new ArrayList<>();
        client.findAll().forEach(ases::add);
        ases.forEach(ase -> {
            int personIn = ThreadLocalRandom.current().nextInt(0, 380); // fetched
            client.saveMeasurement(ase.getId(),
                    new DeltaPeople(null, ase.getId(), ase.getLocation(), personIn, 10));
        });
    }
}
