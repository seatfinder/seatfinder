package seatfinder.fetcher;

import io.micronaut.context.annotation.Requires;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.scheduling.annotation.Scheduled;
import seatfinder.model.measurement.CountClient;
import seatfinder.model.sensor.Wlan;
import seatfinder.service.client.WlanClient;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

@Singleton
@Requires(property = "wlan.fetch")
public class WlanFetcher {

    @Inject
    WlanClient client;

    @Inject
    @Client("/ClientDetails")
    HttpClient wlan;

    @Scheduled(fixedRate = "${ase.fetch.fixedRate:1m}")
    void fetchWlanMeasurement(){
        System.out.println("wlan fetching");
        // storing
        ArrayList<Wlan> wlans = new ArrayList<>();
        client.findAll().forEach(wlans::add);
        wlans.forEach(wlan -> {
            int countClients = ThreadLocalRandom.current().nextInt(0, 160); // fetched
            client.saveMeasurement(wlan.getId(),
                    new CountClient(null, wlan.getId(), wlan.getLocation(), countClients));
        });
    }
}
