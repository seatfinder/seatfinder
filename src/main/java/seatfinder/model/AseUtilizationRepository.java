package seatfinder.model;

import io.micronaut.data.annotation.Query;
import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Slice;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.GenericRepository;
import seatfinder.model.measurement.Utilization;
import seatfinder.model.measurement.UtilizationDTO;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@JdbcRepository(dialect = Dialect.POSTGRES)
public interface AseUtilizationRepository extends GenericRepository<Utilization, ZonedDateTime> {
    @Query(value = "select bucket as time, sum(count_people) as count_people from utilization_ase_minutely where (bucket between :from and :to) and location <@ text2ltree(:location) group by bucket order by bucket desc")
    Slice<UtilizationDTO> findByBucketAndAggregatedSubLocations(@NotNull ZonedDateTime from,@NotNull ZonedDateTime to, @NotNull String location, @NotNull Pageable pageable);
}
