package seatfinder.model;

import io.micronaut.data.annotation.Query;
import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.PageableRepository;
import seatfinder.model.measurement.DeltaPeople;
import seatfinder.model.measurement.DeltaPeopleDTO;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@JdbcRepository(dialect = Dialect.POSTGRES)
public interface DeltaPeopleRepository extends PageableRepository<DeltaPeople, Long> {
    Page<DeltaPeopleDTO> findBySensorIdOrderByTimeDesc(@NotNull Long sensorId, @NotNull Pageable pageable);

    @Query(value = "select time, person_in, person_out from delta_people where cast(time as date) = cast(:time as date) and sensor_id = :sensorId order by time desc",
            countQuery = "select count(*) from delta_people where cast(time as date) = cast(:time as date) and sensor_id = :sensorId")
    Page<DeltaPeopleDTO> findByDateAndSensorIdOrderByTimeDesc(ZonedDateTime time, @NotNull Long sensorId, @NotNull Pageable pageable);
}
