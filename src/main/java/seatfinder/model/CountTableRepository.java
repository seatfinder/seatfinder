package seatfinder.model;

import edu.umd.cs.findbugs.annotations.NonNull;
import io.micronaut.data.annotation.Query;
import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.PageableRepository;
import seatfinder.model.measurement.CountTable;
import seatfinder.model.measurement.CountTableDTO;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@JdbcRepository(dialect = Dialect.POSTGRES)
public interface CountTableRepository extends PageableRepository<CountTable, Long> {
    Page<CountTableDTO> findBySensorIdOrderByTimeDesc(@NonNull Long sensorId, @NonNull Pageable pageable);

    @Query(value = "select time, table_occupied, table_free from count_table where cast(time as date) = cast(:time as date) and sensor_id = :sensorId order by time desc",
            countQuery = "select count(*) from count_table where cast(time as date) = cast(:time as date) and sensor_id = :sensorId")
    Page<CountTableDTO> findByDateAndSensorIdOrderByTimeDesc(ZonedDateTime time, @NotNull Long sensorId, @NotNull Pageable pageable);
}
