package seatfinder.model;

import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.CrudRepository;
import seatfinder.model.sensor.Area;

@JdbcRepository(dialect = Dialect.POSTGRES)
public interface AreaRepository extends CrudRepository<Area, Long> {
}
