package seatfinder.model;

import io.micronaut.data.annotation.Query;
import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.GenericRepository;
import seatfinder.model.measurement.CombinedMeasurement;
import seatfinder.model.measurement.CombinedMeasurementDTO;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@JdbcRepository(dialect = Dialect.POSTGRES)
public interface CombinedMeasurementRepository extends GenericRepository<CombinedMeasurement, ZonedDateTime> {
    @Query(value = "select * from combined_measurement where location = text2ltree(:location)",
            countQuery = "select count(*) from combined_measurement where location = text2ltree(:location)")
    Page<CombinedMeasurementDTO> findByLocation(@NotNull String location, @NotNull Pageable pageable);

    @Query(value = "select * from combined_measurement where location <@ text2ltree(:location)",
            countQuery = "select count(*) from combined_measurement where location <@ text2ltree(:location)")
    Page<CombinedMeasurementDTO> findByLocationWithSubLocations(@NotNull String location, @NotNull Pageable pageable);

    @Query(value = "select * from combined_measurement where cast(time as date) = cast(:time as date) and location = text2ltree(:location)",
            countQuery = "select count(*) from combined_measurement where cast(time as date) = cast(:time as date) and location = text2ltree(:location)")
    Page<CombinedMeasurementDTO> findByTimeAndLocation(ZonedDateTime time, @NotNull String location, @NotNull Pageable pageable);

    @Query(value = "select * from combined_measurement where cast(time as date) = cast(:time as date) and location <@ text2ltree(:location)",
            countQuery = "select count(*) from combined_measurement where cast(time as date) = cast(:time as date) and location <@ text2ltree(:location)")
    Page<CombinedMeasurementDTO> findByTimeAndLocationWithSubLocations(ZonedDateTime time, @NotNull String location, @NotNull Pageable pageable);
}
