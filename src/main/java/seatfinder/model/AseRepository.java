package seatfinder.model;

import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.CrudRepository;
import seatfinder.model.sensor.Ase;

@JdbcRepository(dialect = Dialect.POSTGRES)
public interface AseRepository extends CrudRepository<Ase, Long> {
}
