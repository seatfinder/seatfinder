package seatfinder.model.sensor;

import io.micronaut.data.annotation.MappedEntity;

@MappedEntity
public class Camera extends Sensor {
    public Camera(int deviceId, String location, boolean active) {
        super(deviceId, location, active, "CAMERA");
    }
}
