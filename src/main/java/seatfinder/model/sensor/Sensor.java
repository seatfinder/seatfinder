package seatfinder.model.sensor;

import io.micronaut.data.annotation.GeneratedValue;
import io.micronaut.data.annotation.Id;
import io.micronaut.data.annotation.MappedEntity;
import io.micronaut.data.annotation.MappedProperty;
import io.micronaut.data.jdbc.annotation.ColumnTransformer;

import javax.annotation.Nullable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@MappedEntity
public class Sensor {
    @GeneratedValue
    @Id
    private Long id;

    @MappedProperty("device_id")
    @NotNull
    private final int deviceId;

    @ColumnTransformer(write = "?::ltree")
    @NotBlank
    @NotNull
    private final String location;

    @Nullable
    private final Boolean active;

    @ColumnTransformer(write = "?::sensor_type")
    private String type;

    public Sensor(int deviceId, String location, boolean active, String type) {
        this.deviceId = deviceId;
        this.location = location;
        this.active = active;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getDeviceId() {
        return deviceId;
    }

    public String getLocation() {
        return location;
    }

    public Boolean isActive() {
        return active;
    }

    @Nullable
    public Boolean getActive() {
        return active;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
