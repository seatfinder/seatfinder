package seatfinder.model.sensor;

import io.micronaut.data.annotation.GeneratedValue;
import io.micronaut.data.annotation.Id;
import io.micronaut.data.annotation.MappedEntity;
import io.micronaut.data.jdbc.annotation.ColumnTransformer;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@MappedEntity
public class Area {

    @GeneratedValue
    @Id
    Long id;

    @ColumnTransformer(write = "?::ltree")
    @NotBlank
    @NotNull
    private final String location;

    @NotNull
    private final int capacity_people;

    public Area(@NotBlank @NotNull String location, @NotNull int capacity_people) {
        this.location = location;
        this.capacity_people = capacity_people;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public int getCapacity_people() {
        return capacity_people;
    }
}
