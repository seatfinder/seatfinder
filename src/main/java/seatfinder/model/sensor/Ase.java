package seatfinder.model.sensor;

public class Ase extends Sensor {
    public Ase(int deviceId, String location, boolean active) {
        super(deviceId, location, active, "ASE");
    }
}
