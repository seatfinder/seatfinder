package seatfinder.model.sensor;

import io.micronaut.data.annotation.MappedEntity;

@MappedEntity
public class Wlan extends Sensor {
    public Wlan(int deviceId, String location, boolean active) {
        super(deviceId, location, active, "WLAN");
    }
}
