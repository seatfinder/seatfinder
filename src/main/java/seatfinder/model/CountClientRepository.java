package seatfinder.model;


import io.micronaut.data.annotation.Query;
import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.PageableRepository;
import seatfinder.model.measurement.CountClient;
import seatfinder.model.measurement.CountClientDTO;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@JdbcRepository(dialect = Dialect.POSTGRES)
public interface CountClientRepository extends PageableRepository<CountClient, Long> {
    Page<CountClientDTO> findBySensorIdOrderByTimeDesc(@NotNull Long sensor_id, @NotNull Pageable pageable);

    @Query(value = "select time, count_clients from count_client where cast(time as date) = cast(:time as date) and sensor_id = :sensorId order by time desc",
            countQuery = "select count(*) from count_client where cast(time as date) = cast(:time as date) and sensor_id = :sensorId")
    Page<CountClientDTO> findByDateAndSensorIdOrderByTime(ZonedDateTime time, @NotNull Long sensorId, @NotNull Pageable pageable);
}
