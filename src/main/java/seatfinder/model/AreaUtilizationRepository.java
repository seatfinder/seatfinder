package seatfinder.model;

import io.micronaut.data.annotation.Query;
import io.micronaut.data.jdbc.annotation.JdbcRepository;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Slice;
import io.micronaut.data.model.query.builder.sql.Dialect;
import io.micronaut.data.repository.GenericRepository;
import seatfinder.model.measurement.AreaUtilization;
import seatfinder.model.measurement.AreaUtilizationDTO;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@JdbcRepository(dialect = Dialect.POSTGRES)
public interface AreaUtilizationRepository extends GenericRepository<AreaUtilization, ZonedDateTime> {
    @Query(value = "select bucket as time, type, sum(count_people) as count_people from combined_utilization_minutely where (bucket between :from and :to) and location <@ text2ltree(:location) group by bucket, type order by bucket desc")
    Slice<AreaUtilizationDTO> findByBucketAndAggregatedSubLocations(@NotNull ZonedDateTime from, @NotNull ZonedDateTime to, @NotNull String location, @NotNull Pageable pageable);
}
