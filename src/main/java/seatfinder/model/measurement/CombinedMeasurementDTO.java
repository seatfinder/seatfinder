package seatfinder.model.measurement;

import io.micronaut.data.annotation.MappedEntity;
import io.micronaut.data.annotation.MappedProperty;
import io.micronaut.data.annotation.TypeDef;
import io.micronaut.data.jdbc.annotation.ColumnTransformer;
import io.micronaut.data.model.DataType;

import java.util.Map;

@MappedEntity
public class CombinedMeasurementDTO extends MeasurementDTO {

    @MappedProperty("sensor_id")
    private Long sensorId;

    private String location;

    @TypeDef(type = DataType.JSON)
    private Map<String, Integer> measurement;

    @MappedProperty("sensor_id")
    public Long getSensorId() {
        return sensorId;
    }

    @MappedProperty("sensor_id")
    public void setSensorId(Long sensorId) {
        this.sensorId = sensorId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @TypeDef(type = DataType.JSON)
    public Map<String, Integer> getMeasurement() {
        return measurement;
    }

    @TypeDef(type = DataType.JSON)
    public void setMeasurement(Map<String, Integer> measurement) {
        this.measurement = measurement;
    }
}
