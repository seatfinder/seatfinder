package seatfinder.model.measurement;

import io.micronaut.core.annotation.Introspected;

import java.time.ZonedDateTime;

@Introspected
public class AreaUtilizationDTO extends UtilizationDTO {

    private String type;

    public AreaUtilizationDTO(ZonedDateTime time, Integer countPeople, String type) {
        super(time, countPeople);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
