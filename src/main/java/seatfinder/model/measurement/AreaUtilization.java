package seatfinder.model.measurement;

import io.micronaut.data.annotation.MappedEntity;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@MappedEntity
public class AreaUtilization extends Utilization {

    private String type;

    public AreaUtilization(ZonedDateTime time, String location, @NotNull Long sensorId, Integer countPeople, String type) {
        super(time, sensorId, location, countPeople);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
