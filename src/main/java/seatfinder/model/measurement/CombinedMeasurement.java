package seatfinder.model.measurement;

import io.micronaut.data.annotation.GeneratedValue;
import io.micronaut.data.annotation.Id;
import io.micronaut.data.annotation.MappedEntity;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@MappedEntity
public class CombinedMeasurement extends Measurement {
    @GeneratedValue
    @Id
    private Long id;

    public CombinedMeasurement(Long id, ZonedDateTime time, @NotNull Long sensorId, String location) {
        super(time, sensorId, location);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
