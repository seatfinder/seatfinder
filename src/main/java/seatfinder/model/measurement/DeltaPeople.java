package seatfinder.model.measurement;

import io.micronaut.data.annotation.GeneratedValue;
import io.micronaut.data.annotation.Id;
import io.micronaut.data.annotation.MappedEntity;
import io.micronaut.data.annotation.MappedProperty;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@MappedEntity(value = "delta_people")
public class DeltaPeople extends Measurement {

    @MappedProperty("person_in")
    @NotNull
    private final int personIn;

    @MappedProperty("person_out")
    @NotNull
    private final int personOut;

    @GeneratedValue
    @Id
    private Long id;

    public DeltaPeople(ZonedDateTime time, @NotNull Long sensorId, String location, int personIn, int personOut) {
        super(time, sensorId, location);
        this.personIn = personIn;
        this.personOut = personOut;
    }

    public int getPersonIn() {
        return personIn;
    }

    public int getPersonOut() {
        return personOut;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
