package seatfinder.model.measurement;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.data.annotation.MappedProperty;

import java.time.ZonedDateTime;

@Introspected
public class UtilizationDTO extends MeasurementDTO {

    @MappedProperty("count_people")
    private Integer countPeople;

    public UtilizationDTO(ZonedDateTime time, Integer countPeople) {
        super(time);
        this.countPeople = countPeople;
    }

    public Integer getCountPeople() {
        return countPeople;
    }

    public void setCountPeople(Integer countPeople) {
        this.countPeople = countPeople;
    }
}
