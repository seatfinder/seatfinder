package seatfinder.model.measurement;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.data.annotation.Id;

import java.time.ZonedDateTime;

@Introspected
public class MeasurementDTO {
    @Id
    protected ZonedDateTime time;

    public MeasurementDTO(ZonedDateTime time) {
        this.time = time;
    }

    public MeasurementDTO(){
        time = null;
    }

    public ZonedDateTime getTime() {
        return time;
    }

    public void setTime(ZonedDateTime time) {
        this.time = time;
    }
}
