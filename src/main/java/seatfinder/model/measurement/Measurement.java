package seatfinder.model.measurement;

import io.micronaut.data.annotation.MappedEntity;
import io.micronaut.data.annotation.MappedProperty;
import io.micronaut.data.jdbc.annotation.ColumnTransformer;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;


@MappedEntity
public class Measurement {

    private ZonedDateTime time;

    @MappedProperty("sensor_id")
    @NotNull
    private Long sensorId;

    @ColumnTransformer(write = "?::ltree")
    private final String location;

    public Measurement(ZonedDateTime time, @NotNull Long sensorId, String location) {
        this.time = time;
        this.sensorId = sensorId;
        this.location = location;
    }

    @MappedProperty("sensor_id")
    public Long getSensorId() {
        return sensorId;
    }

    @MappedProperty("sensor_id")
    public void setSensorId(Long sensorId) {
        this.sensorId = sensorId;
    }

    public ZonedDateTime getTime() {
        return time;
    }

    public void setTime(ZonedDateTime time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }
}
