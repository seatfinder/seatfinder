package seatfinder.model.measurement;

import io.micronaut.data.annotation.MappedEntity;
import io.micronaut.data.annotation.MappedProperty;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@MappedEntity
public class Utilization extends Measurement {

    @MappedProperty("count_people")
    private Integer countPeople;

    public Utilization(ZonedDateTime time, @NotNull Long sensorId, String location, Integer countPeople) {
        super(time, sensorId, location);
        this.countPeople = countPeople;
    }

    public Integer getCountPeople() {
        return countPeople;
    }

    public void setCountPeople(Integer countPeople) {
        this.countPeople = countPeople;
    }
}
