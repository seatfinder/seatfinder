package seatfinder.model.measurement;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.data.annotation.MappedProperty;

import java.time.ZonedDateTime;

@Introspected
public class CountTableDTO extends MeasurementDTO {

    @MappedProperty("table_occupied")
    private int tableOccupied;

    @MappedProperty("table_free")
    private int tableFree;

    public CountTableDTO(ZonedDateTime time, int tableOccupied, int tableFree) {
        super(time);
        this.tableOccupied = tableOccupied;
        this.tableFree = tableFree;
    }

    @MappedProperty("table_occupied")
    public int getTableOccupied() {
        return tableOccupied;
    }

    @MappedProperty("table_occupied")
    public void setTableOccupied(int tableOccupied) {
        this.tableOccupied = tableOccupied;
    }

    @MappedProperty("table_free")
    public int getTableFree() {
        return tableFree;
    }

    @MappedProperty("table_free")
    public void setTableFree(int tableFree) {
        this.tableFree = tableFree;
    }
}
