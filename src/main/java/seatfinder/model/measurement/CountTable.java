package seatfinder.model.measurement;

import io.micronaut.data.annotation.GeneratedValue;
import io.micronaut.data.annotation.Id;
import io.micronaut.data.annotation.MappedEntity;
import io.micronaut.data.annotation.MappedProperty;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@MappedEntity(value = "count_table")
public class CountTable extends Measurement {

    @MappedProperty("table_occupied")
    @NotNull
    private final int tableOccupied;

    @MappedProperty("table_free")
    @NotNull
    private final int tableFree;

    @GeneratedValue
    @Id
    private Long id;

    public CountTable(ZonedDateTime time, @NotNull Long sensorId, String location, int tableOccupied, int tableFree) {
        super(time, sensorId, location);
        this.tableOccupied = tableOccupied;
        this.tableFree = tableFree;
    }

    @MappedProperty("table_occupied")
    public int getTableOccupied() {
        return tableOccupied;
    }

    @MappedProperty("table_free")
    public int getTableFree() {
        return tableFree;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
