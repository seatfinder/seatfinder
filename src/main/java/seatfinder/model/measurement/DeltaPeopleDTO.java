package seatfinder.model.measurement;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.data.annotation.MappedProperty;

import java.time.ZonedDateTime;

@Introspected
public class DeltaPeopleDTO extends MeasurementDTO {

    @MappedProperty("person_in")
    private int personIn;

    @MappedProperty("person_out")
    private int personOut;

    public DeltaPeopleDTO(ZonedDateTime time, int personIn, int personOut) {
        super(time);
        this.personIn = personIn;
        this.personOut = personOut;
    }

    @MappedProperty("person_in")
    public int getPersonIn() {
        return personIn;
    }

    @MappedProperty("person_in")
    public void setPersonIn(int personIn) {
        this.personIn = personIn;
    }

    @MappedProperty("person_out")
    public int getPersonOut() {
        return personOut;
    }

    @MappedProperty("person_out")
    public void setPersonOut(int personOut) {
        this.personOut = personOut;
    }
}
