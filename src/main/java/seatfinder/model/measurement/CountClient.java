package seatfinder.model.measurement;

import io.micronaut.data.annotation.GeneratedValue;
import io.micronaut.data.annotation.Id;
import io.micronaut.data.annotation.MappedEntity;
import io.micronaut.data.annotation.MappedProperty;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@MappedEntity("count_client")
public class CountClient extends Measurement {

    @MappedProperty("count_clients")
    private final int countClients;

    @GeneratedValue
    @Id
    private Long id;

    public CountClient(ZonedDateTime time, @NotNull Long sensorId, String location, int countClients) {
        super(time, sensorId, location);
        this.countClients = countClients;
    }

    @MappedProperty("count_clients")
    @NotNull
    public int getCountClients() {
        return countClients;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
