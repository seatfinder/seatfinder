package seatfinder.model.measurement;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.data.annotation.MappedProperty;

import java.time.ZonedDateTime;

@Introspected
public class CountClientDTO extends MeasurementDTO {

    @MappedProperty("count_clients")
    private int countClients;

    public CountClientDTO(ZonedDateTime time, int countClients) {
        super(time);
        this.countClients = countClients;
    }

    @MappedProperty("count_clients")
    public int getCountClients() {
        return countClients;
    }

    @MappedProperty("count_clients")
    public void setCountClients(int countClients) {
        this.countClients = countClients;
    }
}
