package seatfinder.controller;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import seatfinder.model.SensorRepository;
import seatfinder.model.sensor.Sensor;

@Controller("/sensors")
public class SensorController {

    private final SensorRepository repository;

    public SensorController(SensorRepository repository) {
        this.repository = repository;
    }

    @Get
    public Iterable<Sensor> findAll() {
        return repository.findAll();
    }
}
