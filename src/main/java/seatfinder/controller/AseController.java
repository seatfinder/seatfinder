package seatfinder.controller;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Slice;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import seatfinder.model.AseRepository;
import seatfinder.model.AseUtilizationRepository;
import seatfinder.model.DeltaPeopleRepository;
import seatfinder.model.measurement.DeltaPeople;
import seatfinder.model.measurement.DeltaPeopleDTO;
import seatfinder.model.measurement.UtilizationDTO;
import seatfinder.model.sensor.Ase;
import seatfinder.service.RessourceService;

import javax.annotation.Nullable;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Controller("/ases")
public class AseController implements RessourceService<Ase, DeltaPeopleDTO, UtilizationDTO, DeltaPeople> {

    private final AseRepository repository;
    private final DeltaPeopleRepository measurements;
    private final AseUtilizationRepository utilization;

    public AseController(AseRepository repository, DeltaPeopleRepository measurements,
                         AseUtilizationRepository utilization) {
        this.repository = repository;
        this.measurements = measurements;
        this.utilization = utilization;
    }

    @Override
    public Iterable<Ase> findAll() {
        return repository.findAll();
    }

    @Override
    public Ase findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Ase save(@Valid Ase ase) {
        return repository.save(ase);
    }

    @Override
    public HttpResponse delete(Long id) {
        repository.deleteById(id);
        return HttpResponse.ok();
    }

    @Override
    public Ase put(Long id, Ase ase) {
        ase.setId(id);
        return repository.update(ase);
    }

    @Override
    public Page<DeltaPeopleDTO> findAllMeasurements(Long id, LocalDate date, Boolean subareas, Pageable pageable) {
        if (date != null) {
            ZonedDateTime time = date.atStartOfDay(ZoneId.systemDefault());
            return measurements.findByDateAndSensorIdOrderByTimeDesc(time, id, pageable);
        } else {
            return measurements.findBySensorIdOrderByTimeDesc(id, pageable);
        }
    }

    @Override
    public Slice<UtilizationDTO> findAllUtilizations(LocalDate date, @Nullable String area, @Valid Pageable pageable) {
        if (date == null)
            date = LocalDate.now();
        ZonedDateTime from = date.atStartOfDay(ZoneId.systemDefault());
        ZonedDateTime to = date.atStartOfDay(ZoneId.systemDefault()).plusDays(1).minusSeconds(1);
        return utilization.findByBucketAndAggregatedSubLocations(from, to, area, pageable);
    }

    @Override
    public DeltaPeople saveMeasurement(Long id, DeltaPeople ressource) {
        ressource.setSensorId(id);
        if(ressource.getTime() == null) ressource.setTime(ZonedDateTime.now());
        return measurements.save(ressource);
    }
}
