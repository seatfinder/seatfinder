package seatfinder.controller;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.annotation.Controller;
import seatfinder.model.CountClientRepository;
import seatfinder.model.measurement.CountClient;
import seatfinder.model.measurement.CountClientDTO;
import seatfinder.service.MeasurementService;

@Controller("/countclients")
public class CountClientController implements MeasurementService<CountClient, CountClientDTO> {

    private final CountClientRepository repository;

    public CountClientController(CountClientRepository repository) {
        this.repository = repository;
    }

    @Override
    public CountClient save(CountClient countClient) {
        return repository.save(countClient);
    }

    @Override
    public Page<CountClient> listAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Page<CountClientDTO> listAllBySensorId(Long sensorId, Pageable pageable) {
        return repository.findBySensorIdOrderByTimeDesc(sensorId, pageable);
    }
}
