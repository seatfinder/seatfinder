package seatfinder.controller;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Slice;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import seatfinder.model.CameraRepository;
import seatfinder.model.CameraUtilizationRepository;
import seatfinder.model.CountTableRepository;
import seatfinder.model.measurement.CountTable;
import seatfinder.model.measurement.CountTableDTO;
import seatfinder.model.measurement.UtilizationDTO;
import seatfinder.model.sensor.Camera;
import seatfinder.service.RessourceService;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Controller("/cameras")
public class CameraController implements RessourceService<Camera, CountTableDTO, UtilizationDTO, CountTable> {

    private final CameraRepository repository;
    private final CountTableRepository measurements;
    private final CameraUtilizationRepository utilization;

    public CameraController(CameraRepository repository, CountTableRepository measurements,
                            CameraUtilizationRepository utilization) {
        this.repository = repository;
        this.measurements = measurements;
        this.utilization = utilization;
    }

    @Override
    public Iterable<Camera> findAll() {
        return repository.findAll();
    }

    @Override
    public Camera findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Camera save(@Body Camera camera) {
        return repository.save(camera);
    }

    @Override
    public HttpResponse delete(Long id) {
        repository.deleteById(id);
        return HttpResponse.ok();
    }

    @Override
    public Camera put(Long id, @Body Camera camera) {
        camera.setId(id);
        return repository.update(camera);
    }

    @Override
    public Page<CountTableDTO> findAllMeasurements(Long id, LocalDate date, Boolean subareas, Pageable pageable) {
        if (date != null) {
            ZonedDateTime time = date.atStartOfDay(ZoneId.systemDefault());
            return measurements.findByDateAndSensorIdOrderByTimeDesc(time, id, pageable);
        } else {
            return measurements.findBySensorIdOrderByTimeDesc(id, pageable);
        }
    }

    @Override
    public Slice<UtilizationDTO> findAllUtilizations(LocalDate date, String area, Pageable pageable) {
        if (date == null)
            date = LocalDate.now();
        ZonedDateTime from = date.atStartOfDay(ZoneId.systemDefault());
        ZonedDateTime to = date.atStartOfDay(ZoneId.systemDefault()).plusDays(1).minusSeconds(1);
        return utilization.findByBucketAndAggregatedSubLocations(from, to, area, pageable);
    }

    @Override
    public CountTable saveMeasurement(Long id, CountTable ressource) {
        ressource.setSensorId(id);
        if(ressource.getTime() == null) ressource.setTime(ZonedDateTime.now());
        return measurements.save(ressource);
    }
}
