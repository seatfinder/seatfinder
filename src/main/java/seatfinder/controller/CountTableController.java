package seatfinder.controller;

import edu.umd.cs.findbugs.annotations.NonNull;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import seatfinder.model.CountTableRepository;
import seatfinder.model.measurement.CountTable;
import seatfinder.model.measurement.CountTableDTO;
import seatfinder.service.MeasurementService;

import javax.validation.constraints.NotNull;

@Controller("/counttables")
public class CountTableController implements MeasurementService<CountTable, CountTableDTO> {

    private final CountTableRepository repository;

    public CountTableController(CountTableRepository repository) {
        this.repository = repository;
    }

    @Override
    public CountTable save(@Body CountTable countTable) {
        return repository.save(countTable);
    }

    @Override
    public Page<CountTable> listAll(@NonNull Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Page<CountTableDTO> listAllBySensorId(@NonNull Long sensorId, @NotNull Pageable pageable) {
        return repository.findBySensorIdOrderByTimeDesc(sensorId, pageable);
    }
}

