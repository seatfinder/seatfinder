package seatfinder.controller;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Slice;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import seatfinder.model.CountClientRepository;
import seatfinder.model.WlanRepository;
import seatfinder.model.WlanUtilizationRepository;
import seatfinder.model.measurement.CountClient;
import seatfinder.model.measurement.CountClientDTO;
import seatfinder.model.measurement.UtilizationDTO;
import seatfinder.model.sensor.Wlan;
import seatfinder.service.RessourceService;

import javax.annotation.Nullable;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Controller("/wlans")
public class WlanController implements RessourceService<Wlan, CountClientDTO, UtilizationDTO, CountClient> {

    private final WlanRepository repository;
    private final CountClientRepository measurements;
    private final WlanUtilizationRepository utilization;

    public WlanController(WlanRepository repository, CountClientRepository measurements,
                          WlanUtilizationRepository utilization) {
        this.repository = repository;
        this.measurements = measurements;
        this.utilization = utilization;
    }

    @Override
    public Iterable<Wlan> findAll() {
        return repository.findAll();
    }

    @Override
    public Wlan findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Wlan save(Wlan wlan) {
        return repository.save(wlan);
    }

    @Override
    public HttpResponse delete(Long id) {
        repository.deleteById(id);
        return HttpResponse.ok();
    }

    @Override
    public Wlan put(Long id, Wlan wlan) {
        wlan.setId(id);
        return repository.update(wlan);
    }

    @Override
    public Page<CountClientDTO> findAllMeasurements(Long id, LocalDate date, Boolean subareas, Pageable pageable) {
        if (date != null) {
            ZonedDateTime time = date.atStartOfDay(ZoneId.systemDefault());
            return measurements.findByDateAndSensorIdOrderByTime(time, id, pageable);
        } else {
            return measurements.findBySensorIdOrderByTimeDesc(id, pageable);
        }
    }

    @Override
    public Slice<UtilizationDTO> findAllUtilizations(LocalDate date, @Nullable String area, @Valid Pageable pageable) {
        if(date == null)
            date = LocalDate.now();
        ZonedDateTime from = date.atStartOfDay(ZoneId.systemDefault());
        ZonedDateTime to = date.atStartOfDay(ZoneId.systemDefault()).plusDays(1).minusSeconds(1);
        return utilization.findByBucketAndAggregatedSubLocations(from, to, area, pageable);
    }

    @Override
    public CountClient saveMeasurement(Long id, CountClient ressource) {
        ressource.setSensorId(id);
        if(ressource.getTime() == null) ressource.setTime(ZonedDateTime.now());
        return measurements.save(ressource);
    }

}
