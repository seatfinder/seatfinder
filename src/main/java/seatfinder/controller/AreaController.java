package seatfinder.controller;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Slice;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import seatfinder.model.AreaRepository;
import seatfinder.model.AreaUtilizationRepository;
import seatfinder.model.CombinedMeasurementRepository;
import seatfinder.model.measurement.AreaUtilizationDTO;
import seatfinder.model.measurement.CombinedMeasurementDTO;
import seatfinder.model.measurement.Measurement;
import seatfinder.model.sensor.Area;
import seatfinder.service.RessourceService;

import javax.annotation.Nullable;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

@Controller("/areas")
public class AreaController implements RessourceService<Area, CombinedMeasurementDTO, AreaUtilizationDTO, Measurement> {
    private final AreaRepository repository;
    private final CombinedMeasurementRepository measurement;
    private final AreaUtilizationRepository utilization;

    public AreaController(AreaRepository repository, CombinedMeasurementRepository measurement,
                          AreaUtilizationRepository utilization) {
        this.repository = repository;
        this.measurement = measurement;
        this.utilization = utilization;
    }

    @Override
    public Iterable<Area> findAll() {
        return repository.findAll();
    }

    @Override
    public Area findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Area save(@Body Area area) {
        return repository.save(area);
    }

    @Override
    public HttpResponse delete(Long id) {
        repository.deleteById(id);
        return HttpResponse.ok();
    }

    @Override
    public Area put(Long id, Area area) {
        area.setId(id);
        return repository.update(area);
    }

    @Override
    public Page<CombinedMeasurementDTO> findAllMeasurements(Long id, LocalDate date, Boolean subareas, Pageable pageable) {
        Optional<Area> area = repository.findById(id);
        if (area.isPresent()) {
            if (!subareas) {
                if (date != null) {
                    ZonedDateTime time = date.atStartOfDay(ZoneId.systemDefault());
                    return measurement.findByTimeAndLocation(time, area.get().getLocation(), pageable);
                } else {
                    return measurement.findByLocation(area.get().getLocation(), pageable);
                }
            } else {
                if (date != null) {
                    ZonedDateTime time = date.atStartOfDay(ZoneId.systemDefault());
                    return measurement.findByTimeAndLocationWithSubLocations(time, area.get().getLocation(), pageable);
                } else {
                    return measurement.findByLocationWithSubLocations(area.get().getLocation(), pageable);
                }
            }
        } else {
            return Page.<CombinedMeasurementDTO>empty();
        }
    }

    @Override
    public Slice<AreaUtilizationDTO> findAllUtilizations(LocalDate date, @Nullable String area,
                                                         @Valid Pageable pageable) {
        if(date == null)
            date = LocalDate.now();
        ZonedDateTime from = date.atStartOfDay(ZoneId.systemDefault());
        ZonedDateTime to = date.atStartOfDay(ZoneId.systemDefault()).plusDays(1).minusSeconds(1);
        return utilization.findByBucketAndAggregatedSubLocations(from, to, area, pageable);
    }

    @Override
    public Measurement saveMeasurement(Long id, Measurement ressource) {
        return null; // @TODO: not supported, needs refactoring of return type to HTTPResponse<Measurement>
    }
}