package seatfinder.controller;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.annotation.Controller;
import seatfinder.model.DeltaPeopleRepository;
import seatfinder.model.measurement.DeltaPeople;
import seatfinder.model.measurement.DeltaPeopleDTO;
import seatfinder.service.MeasurementService;

@Controller("/deltapeople")
public class DeltaPeopleController implements MeasurementService<DeltaPeople, DeltaPeopleDTO> {

    private final DeltaPeopleRepository repository;

    public DeltaPeopleController(DeltaPeopleRepository repository) {
        this.repository = repository;
    }

    @Override
    public DeltaPeople save(DeltaPeople deltaPeople) {
        return repository.save(deltaPeople);
    }

    @Override
    public Page<DeltaPeople> listAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Page<DeltaPeopleDTO> listAllBySensorId(Long sensorId, Pageable pageable) {
        return repository.findBySensorIdOrderByTimeDesc(sensorId, pageable);
    }
}
