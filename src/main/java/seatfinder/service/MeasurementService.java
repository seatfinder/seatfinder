package seatfinder.service;

import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface MeasurementService<M, DTO> {
    @Post
    M save(@Body @Valid M measurement);

    @Get
    Page<M> listAll(@NotNull Pageable pageable);

    @Get("/{sensorId}")
    Page<DTO> listAllBySensorId(@NotNull Long sensorId, @NotNull Pageable pageable);
}
