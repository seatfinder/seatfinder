package seatfinder.service;

import io.micronaut.core.convert.format.Format;
import io.micronaut.data.annotation.Id;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.model.Slice;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.validation.Validated;
import seatfinder.model.measurement.Measurement;
import seatfinder.model.measurement.MeasurementDTO;

import javax.annotation.Nullable;
import javax.validation.Valid;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;

@Validated
public interface RessourceService<R, M extends MeasurementDTO, M2 extends MeasurementDTO, M3 extends Measurement> {

    @Get
    Iterable<R> findAll();

    @Get("/{id}")
    R findById(@Id Long id);

    @Post
    R save(@Body @Valid R ressource);

    @Delete("/{id}")
    HttpResponse delete(@Id Long id);

    @Put("/{id}")
    R put(@Id Long id, @Body @Valid R ressource);

    @Get("/{id}/measurements{?date,subareas,pageable*}")
    Page<M> findAllMeasurements(@Id Long id, @Nullable @Format("yyyy-MM-dd") @PastOrPresent LocalDate date,
                                @Nullable @QueryValue(defaultValue = "false") Boolean subareas,
                                @Valid Pageable pageable);

    @Get("/utilizations{?date,area,pageable*}")
    Slice<M2> findAllUtilizations(@Nullable @Format("yyyy-MM-dd") @PastOrPresent LocalDate date,
                                  @Nullable @QueryValue(defaultValue = "HSB") String area, @Valid Pageable pageable);

    @Post("/{id}/measurements")
    M3 saveMeasurement(Long id, @Body M3 ressource);
}
