package seatfinder.service.client;

import io.micronaut.http.client.annotation.Client;
import seatfinder.model.measurement.CountClient;
import seatfinder.model.measurement.CountClientDTO;
import seatfinder.model.measurement.UtilizationDTO;
import seatfinder.model.sensor.Wlan;
import seatfinder.service.RessourceService;

@Client(value = "/wlans")
public interface WlanClient extends RessourceService<Wlan, CountClientDTO, UtilizationDTO, CountClient> {
}
