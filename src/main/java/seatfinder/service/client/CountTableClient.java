package seatfinder.service.client;

import io.micronaut.http.client.annotation.Client;
import seatfinder.model.measurement.CountTable;
import seatfinder.model.measurement.CountTableDTO;
import seatfinder.service.MeasurementService;

@Client("/counttables")
public interface CountTableClient extends MeasurementService<CountTable, CountTableDTO> {
}
