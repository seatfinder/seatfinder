package seatfinder.service.client;

import io.micronaut.http.client.annotation.Client;
import seatfinder.model.measurement.DeltaPeople;
import seatfinder.model.measurement.DeltaPeopleDTO;
import seatfinder.model.measurement.UtilizationDTO;
import seatfinder.model.sensor.Ase;
import seatfinder.service.RessourceService;

@Client(value = "/ases")
public interface AseClient extends RessourceService<Ase, DeltaPeopleDTO, UtilizationDTO, DeltaPeople> {
}
