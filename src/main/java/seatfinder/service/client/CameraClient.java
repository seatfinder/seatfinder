package seatfinder.service.client;

import io.micronaut.http.client.annotation.Client;
import seatfinder.model.measurement.CountTable;
import seatfinder.model.measurement.CountTableDTO;
import seatfinder.model.measurement.UtilizationDTO;
import seatfinder.model.sensor.Camera;
import seatfinder.service.RessourceService;

@Client(value = "/cameras")
public interface CameraClient extends RessourceService<Camera, CountTableDTO, UtilizationDTO, CountTable> {
}