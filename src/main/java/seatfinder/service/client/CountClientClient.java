package seatfinder.service.client;

import io.micronaut.http.client.annotation.Client;
import seatfinder.model.measurement.CountClient;
import seatfinder.model.measurement.CountClientDTO;
import seatfinder.service.MeasurementService;

@Client("/countclients")
public interface CountClientClient extends MeasurementService<CountClient, CountClientDTO> {
}
