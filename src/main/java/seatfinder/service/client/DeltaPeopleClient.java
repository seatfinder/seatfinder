package seatfinder.service.client;

import io.micronaut.http.client.annotation.Client;
import seatfinder.model.measurement.DeltaPeople;
import seatfinder.model.measurement.DeltaPeopleDTO;
import seatfinder.service.MeasurementService;

@Client("/deltapeople")
public interface DeltaPeopleClient extends MeasurementService<DeltaPeople, DeltaPeopleDTO> {
}
