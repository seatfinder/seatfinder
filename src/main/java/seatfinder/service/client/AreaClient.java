package seatfinder.service.client;

import io.micronaut.http.client.annotation.Client;
import seatfinder.model.measurement.AreaUtilizationDTO;
import seatfinder.model.measurement.Measurement;
import seatfinder.model.measurement.MeasurementDTO;
import seatfinder.model.sensor.Area;
import seatfinder.service.RessourceService;

@Client(value = "/areas")
public interface AreaClient extends RessourceService<Area, MeasurementDTO, AreaUtilizationDTO, Measurement> {
}
