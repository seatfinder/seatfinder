package seatfinder;

import io.micronaut.core.annotation.TypeHint;
import io.micronaut.runtime.Micronaut;
import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.info.*;

@OpenAPIDefinition(
    info = @Info(
            title = "seatfinder",
            version = "0.0"
    )
)
@TypeHint(typeNames = {"org.postgresql.Driver","org.postgresql.PGProperty"},
        accessType = {TypeHint.AccessType.ALL_PUBLIC})
public class Application {

    public static void main(String[] args) {
        Micronaut.run(Application.class);
    }
}