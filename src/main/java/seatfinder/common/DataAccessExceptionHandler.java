package seatfinder.common;

import io.micronaut.context.annotation.Requires;
import io.micronaut.data.exceptions.DataAccessException;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.server.exceptions.ExceptionHandler;

import javax.inject.Singleton;
import java.util.HashMap;

@Produces
@Singleton
@Requires(classes = {DataAccessException.class, ExceptionHandler.class})
public class DataAccessExceptionHandler implements ExceptionHandler<DataAccessException, HttpResponse>{
    @Override
    public HttpResponse handle(HttpRequest request, DataAccessException exception) {
        HashMap<String, Object> body = new HashMap<>();
        body.put("message", exception.getMessage());
        return HttpResponse.badRequest(body);
    }
}
