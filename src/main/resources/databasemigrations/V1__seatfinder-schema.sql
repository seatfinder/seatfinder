--CREATE DATABASE seatfinder;

CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;
CREATE EXTENSION IF NOT EXISTS ltree;

SET TIMEZONE = 'Europe/Zurich';

CREATE TABLE area
(
    id              SMALLSERIAL,
    location        ltree    NOT NULL UNIQUE,
    capacity_people SMALLINT NOT NULL
);

CREATE TYPE sensor_type AS ENUM ('CAMERA', 'ASE', 'WLAN');

CREATE TABLE sensor
(
    id        SMALLSERIAL PRIMARY KEY,
    device_id SMALLINT    NOT NULL,
    location  ltree       NOT NULL,
    active    bool        NOT NULL DEFAULT TRUE,
    type      sensor_type NOT NULL
);


CREATE OR REPLACE FUNCTION check_location()
    RETURNS trigger
    LANGUAGE plpgsql
AS
$$
DECLARE
    incoming_location     ltree;
    parent_or_child_exist bool;
BEGIN
    IF NEW.active
    THEN
        SELECT NEW.location INTO incoming_location;
        EXECUTE format(
                'SELECT EXISTS(SELECT s.location FROM %I.%I s '
                    'WHERE s.active and (s.location <@ $1 or s.location @> $1))',
                TG_TABLE_SCHEMA, TG_TABLE_NAME) INTO parent_or_child_exist USING incoming_location;
        IF parent_or_child_exist
        THEN
            RAISE EXCEPTION 'location % already monitored', incoming_location;
        END IF;
    END IF;
    RETURN NEW;
END;
$$;

CREATE TABLE camera
(
    UNIQUE (id, location),
    FOREIGN KEY (location) REFERENCES area (location) ON DELETE RESTRICT
) INHERITS (sensor);
CREATE TRIGGER insert_camera
    BEFORE INSERT OR UPDATE
    ON "camera"
    FOR EACH ROW
EXECUTE PROCEDURE check_location();

ALTER TABLE camera
    ALTER COLUMN type SET DEFAULT 'CAMERA';

CREATE TABLE ase
(
    UNIQUE (id, location),
    FOREIGN KEY (location) REFERENCES area (location) ON DELETE RESTRICT
) INHERITS (sensor);
CREATE TRIGGER insert_ase
    BEFORE INSERT OR UPDATE
    ON "ase"
    FOR EACH ROW
EXECUTE PROCEDURE check_location();

ALTER TABLE ase
    ALTER COLUMN type SET DEFAULT 'ASE';

CREATE TABLE wlan
(
    UNIQUE (id, location),
    FOREIGN KEY (location) REFERENCES area (location) ON DELETE RESTRICT
) INHERITS (sensor);
CREATE TRIGGER insert_wlan
    BEFORE INSERT OR UPDATE
    ON "wlan"
    FOR EACH ROW
EXECUTE PROCEDURE check_location();

ALTER TABLE wlan
    ALTER COLUMN type SET DEFAULT 'WLAN';

--TODO: chunk policy
--TODO: test foreign keys when inserting
CREATE TABLE count_table
(
    id             BIGSERIAL   NOT NULL,
    time           timestamptz DEFAULT NOW(),
    sensor_id      SMALLINT    NOT NULL,
    location       ltree                DEFAULT NULL,
    table_occupied SMALLINT    NOT NULL,
    table_free     SMALLINT    NOT NULL,
    FOREIGN KEY (sensor_id, location) REFERENCES camera (id, location) ON DELETE RESTRICT
);

SELECT create_hypertable('count_table', 'time');
CREATE INDEX camera_location_idx ON count_table USING gist (location);
CREATE INDEX camera_sensor_idx ON count_table (sensor_id);

CREATE OR REPLACE FUNCTION insert_count_table() RETURNS TRIGGER AS
$$
DECLARE
    derived_location ltree;
    is_active        bool;
BEGIN
    SELECT camera.active FROM camera WHERE camera.id = NEW.sensor_id INTO is_active;
    IF NOT is_active
    THEN
        RAISE EXCEPTION 'sensor % not active', NEW.sensor_id;
    END IF;
    IF NEW.location IS NULL
    THEN
        SELECT camera.location FROM camera WHERE camera.active and camera.id = NEW.sensor_id INTO derived_location;
        NEW.location := derived_location;
    END IF;
    IF NEW.time IS NULL
    THEN
        NEW.time = NOW();
    END IF;
    RETURN NEW;
END
$$
    language plpgsql;

CREATE TRIGGER insert_count_table
    BEFORE INSERT OR UPDATE
    ON "count_table"
    FOR EACH ROW
EXECUTE PROCEDURE insert_count_table();


--TODO: chunk policy
--TODO: test foreign keys when inserting
CREATE TABLE delta_people
(
    id         BIGSERIAL   NOT NULL,
    time       timestamptz DEFAULT NOW(),
    sensor_id  SMALLINT    NOT NULL,
    location   ltree                DEFAULT NULL,
    person_in  SMALLINT    NOT NULL,
    person_out SMALLINT    NOT NULL,
    FOREIGN KEY (sensor_id, location) REFERENCES ase (id, location) ON DELETE RESTRICT
);

SELECT create_hypertable('delta_people', 'time');
CREATE INDEX ase_location_idx ON delta_people USING gist (location);
CREATE INDEX ase_sensor_idx ON delta_people (sensor_id);


CREATE OR REPLACE FUNCTION insert_delta_people() RETURNS TRIGGER AS
$$
DECLARE
    derived_location ltree;
    is_active        bool;
BEGIN
    SELECT ase.active FROM ase WHERE ase.id = NEW.sensor_id INTO is_active;
    IF NOT is_active
    THEN
        RAISE EXCEPTION 'sensor % not active', NEW.sensor_id;
    END IF;
    IF NEW.location IS NULL
    THEN
        SELECT ase.location FROM ase WHERE ase.active and ase.id = NEW.sensor_id INTO derived_location;
        NEW.location := derived_location;
    END IF;
    IF NEW.time IS NULL
    THEN
        NEW.time = NOW();
    END IF;
    RETURN NEW;
END
$$
    language plpgsql;

CREATE TRIGGER insert_delta_people
    BEFORE INSERT OR UPDATE
    ON "delta_people"
    FOR EACH ROW
EXECUTE PROCEDURE insert_delta_people();

CREATE TABLE cumsum_people
(
    id        BIGSERIAL   NOT NULL,
    time      timestamptz DEFAULT NOW(),
    sensor_id SMALLINT    NOT NULL,
    location  ltree                DEFAULT NULL,
    sum       SMALLINT,
    FOREIGN KEY (sensor_id, location) REFERENCES ase (id, location) ON DELETE RESTRICT
);

SELECT create_hypertable('cumsum_people', 'time');
CREATE INDEX cumsum_people_location_idx ON cumsum_people USING gist (location);
CREATE INDEX cumsum_people_sensor_idx ON cumsum_people (sensor_id);

CREATE OR REPLACE FUNCTION insert_cumsum() RETURNS TRIGGER AS
$$
DECLARE
    new_sum          SMALLINT;
    derived_location ltree;
    is_active        bool;
BEGIN
    SELECT ase.active FROM ase WHERE ase.id = NEW.sensor_id INTO is_active;
    IF NOT is_active
    THEN
        RAISE EXCEPTION 'sensor % not active', NEW.sensor_id;
    END IF;
    IF NEW.location IS NULL
    THEN
        SELECT ase.location FROM ase WHERE ase.active and ase.id = NEW.sensor_id INTO derived_location;
        NEW.location := derived_location;
    END IF;
    IF NEW.time IS NULL
    THEN
        NEW.time := NOW();
    END IF;
    SELECT last(cumulative_sum, time)
    FROM (
             SELECT sum(person_in - person_out) OVER (ORDER BY time) AS cumulative_sum, time
             FROM delta_people
             WHERE delta_people.sensor_id = NEW.sensor_id
               and delta_people.time::date = NEW.time::date
         ) as cst
    INTO new_sum;
    IF new_sum IS NOT NULL
    THEN
        INSERT INTO cumsum_people(time, sensor_id, location, sum)
        VALUES (NEW.time, NEW.sensor_id, NEW.location, new_sum);
    END IF;
    RETURN NEW;
END;
$$
    language plpgsql;

CREATE TRIGGER after_insert_delta
    AFTER INSERT
    ON delta_people
    FOR EACH ROW
EXECUTE PROCEDURE insert_cumsum();


--TODO: chunk policy
--TODO: test foreign keys when inserting
CREATE TABLE count_client
(
    id            BIGSERIAL   NOT NULL,
    time          timestamptz DEFAULT NOW(),
    sensor_id     SMALLINT    NOT NULL,
    location      ltree                DEFAULT NULL,
    count_clients SMALLINT    NOT NULL,
    FOREIGN KEY (sensor_id, location) REFERENCES wlan (id, location) ON DELETE RESTRICT
);

SELECT create_hypertable('count_client', 'time');
CREATE INDEX wlan_location_idx ON count_client USING gist (location);
CREATE INDEX wlan_sensor_idx ON count_client (sensor_id);


CREATE OR REPLACE FUNCTION insert_count_client() RETURNS TRIGGER AS
$$
DECLARE
    derived_location ltree;
    is_active        bool;
BEGIN
    SELECT wlan.active FROM wlan WHERE wlan.id = NEW.sensor_id INTO is_active;
    IF NOT is_active
    THEN
        RAISE EXCEPTION 'sensor % not active', NEW.sensor_id;
    END IF;
    IF NEW.location IS NULL
    THEN
        SELECT wlan.location FROM wlan WHERE wlan.active and wlan.id = NEW.sensor_id INTO derived_location;
        NEW.location := derived_location;
    END IF;
    IF NEW.time IS NULL
    THEN
        NEW.time = NOW();
    END IF;
    RETURN NEW;
END
$$
    language plpgsql;

CREATE TRIGGER insert_count_client
    BEFORE INSERT OR UPDATE
    ON "count_client"
    FOR EACH ROW
EXECUTE PROCEDURE insert_count_client();



CREATE VIEW utilization_camera_minutely
    WITH (timescaledb.continuous) AS
(
SELECT time_bucket(INTERVAL '1 minute', time) as bucket,
       location                               as location,
       last(sensor_id, time)                  as sensor_id,
       last(table_occupied, time)             as count_people
FROM count_table
GROUP BY bucket, location);


CREATE VIEW utilization_ase_minutely
    WITH (timescaledb.continuous) AS
(
SELECT time_bucket(INTERVAL '1 minute', time) as bucket,
       location                               as location,
       last(sensor_id, time)                  as sensor_id,
       last(sum, time)                        as count_people
FROM cumsum_people
GROUP BY bucket, location);


CREATE VIEW utilization_wlan_minutely
    WITH (timescaledb.continuous) AS
(
SELECT time_bucket(INTERVAL '1 minute', time) as bucket,
       location                               as location,
       last(sensor_id, time)                  as sensor_id,
       last(count_clients, time)              as count_people
FROM count_client
GROUP BY bucket, location);


CREATE VIEW combined_utilization_minutely AS
SELECT *
FROM (
         SELECT *, 'CAMERA' as type
         FROM utilization_camera_minutely
         UNION ALL
         SELECT *, 'ASE' as type
         FROM utilization_ase_minutely
         UNION ALL
         SELECT *, 'WLAN' as type
         FROM utilization_wlan_minutely
     ) as combined
GROUP BY bucket, location, sensor_id, count_people, type
ORDER BY bucket DESC, location DESC;


CREATE VIEW combined_measurement AS
SELECT id, time, location, sensor_id, measurement
FROM (
         SELECT id,
                time,
                location,
                sensor_id,
                jsonb_build_object('table_occupied', table_occupied, 'table_free', table_free) as measurement
         FROM count_table
         UNION ALL
         SELECT id,
                time,
                location,
                sensor_id,
                jsonb_build_object('person_in', person_in, 'person_out', person_out) as measurement
         FROM delta_people
         UNION ALL
         SELECT id, time, location, sensor_id, jsonb_build_object('count_clients', count_clients) as measurement
         FROM count_client
     ) as combined
GROUP BY time, location, sensor_id, id, measurement
ORDER BY time DESC, location DESC;

CREATE TABLE forecast_wlan
(
    id            BIGSERIAL   NOT NULL,
    time          timestamptz DEFAULT NOW(),
    location      ltree                DEFAULT NULL,
    count_people_15min SMALLINT    NOT NULL,
    count_people_30min SMALLINT    NOT NULL,
    count_people_60min SMALLINT    NOT NULL,
    FOREIGN KEY (location) REFERENCES area (location) ON DELETE RESTRICT
);

SELECT create_hypertable('forecast_wlan', 'time');
CREATE INDEX forecast_wlan_location_idx ON forecast_wlan USING gist (location);

CREATE TABLE forecast_ase
(
    id            BIGSERIAL   NOT NULL,
    time          timestamptz DEFAULT NOW(),
    location      ltree                DEFAULT NULL,
    count_people_15min SMALLINT    NOT NULL,
    count_people_30min SMALLINT    NOT NULL,
    count_people_60min SMALLINT    NOT NULL,
    FOREIGN KEY (location) REFERENCES area (location) ON DELETE RESTRICT
);

SELECT create_hypertable('forecast_ase', 'time');
CREATE INDEX forecast_ase_location_idx ON forecast_ase USING gist (location);

CREATE TABLE forecast_camera
(
    id            BIGSERIAL   NOT NULL,
    time          timestamptz DEFAULT NOW(),
    location      ltree                DEFAULT NULL,
    count_people_15min SMALLINT    NOT NULL,
    count_people_30min SMALLINT    NOT NULL,
    count_people_60min SMALLINT    NOT NULL,
    FOREIGN KEY (location) REFERENCES area (location) ON DELETE RESTRICT
);

SELECT create_hypertable('forecast_camera', 'time');
CREATE INDEX forecast_camera_location_idx ON forecast_camera USING gist (location);
