package seatfinder.controller;

import io.micronaut.context.ApplicationContext;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.MutableHttpRequest;
import io.micronaut.http.client.DefaultHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import seatfinder.model.measurement.CountClient;
import seatfinder.model.measurement.CountTable;
import seatfinder.model.measurement.DeltaPeople;
import seatfinder.model.sensor.Area;
import seatfinder.model.sensor.Ase;
import seatfinder.model.sensor.Camera;
import seatfinder.model.sensor.Wlan;
import seatfinder.service.client.AreaClient;
import seatfinder.service.client.AseClient;
import seatfinder.service.client.CameraClient;
import seatfinder.service.client.WlanClient;

import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@MicronautTest
class LifecycleTest {

    @Inject
    CameraClient camera;

    @Inject
    WlanClient wlan;

    @Inject
    AseClient ase;

    @Inject
    AreaClient area;

    @Inject
    EmbeddedServer server;

    @Inject
    ApplicationContext context;

    @Inject
    Flyway flyway;

    @Inject
    @Client("/areas/utilizations")
    DefaultHttpClient utilization;

    @Test
    void saveMeasurement() {
        Area areaCreated = area.save(new Area("TEST.AREA", 120));
        Camera cameraCreated = camera.save(new Camera(1, "TEST.AREA", true));
        Wlan wlanCreated = wlan.save(new Wlan(1, "TEST.AREA", true));
        Ase aseCreated = ase.save(new Ase(1, "TEST.AREA", true));

        ZonedDateTime now = ZonedDateTime.now();
        camera.saveMeasurement(
                cameraCreated.getId(),
                new CountTable(now, cameraCreated.getId(), cameraCreated.getLocation(), 5, 3));
        wlan.saveMeasurement(
                wlanCreated.getId(),
                new CountClient(now, wlanCreated.getId(), wlanCreated.getLocation(), 5));
        ase.saveMeasurement(
                aseCreated.getId(),
                new DeltaPeople(now, aseCreated.getId(), aseCreated.getLocation(), 5, 0));

        //https://github.com/micronaut-projects/micronaut-data/issues/219
        //camera.findAllMeasurements(cameraCreated.getId(),null,false,Pageable.from(0,10, Sort.of(Sort.Order.asc("time"))));
        //instead make a HTTP Request yourself -> default return type Map -> blocking due to Netty server
        MutableHttpRequest<Object> request = HttpRequest.GET(String.format("?date=%s&area=%s&page=0",
                now.toLocalDate(),
                areaCreated.getLocation()));
        Map response = utilization.retrieve(request, Map.class).blockingFirst();

        Assertions.assertEquals(3,
                ((ArrayList)response.get("content")).size()); // 3 sensor types for area 'TEST.AREA'

        Assertions.assertIterableEquals(
                Arrays.asList(5, 5, 5),
                (Iterable<?>) ((ArrayList)response.get("content"))
                        .stream()
                        .map(o -> ((LinkedHashMap)o).get("countPeople"))
                        .collect(Collectors.toList())); // all sensor types count 5 people for 'TEST.AREA'
    }
}