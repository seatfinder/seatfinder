# Seatfinder

## Presentation
[link](https://docs.google.com/presentation/d/14Tagxsm9ZlcnPrVyfa_A_c0tiLsvfVH5Y4-Eh65pZuY/edit?usp=sharing)

## MadLib
/usr/local/madlib/bin/madpack -s madlib -p postgres -c aleksandarspasojevic@localhost/seatfinder install


## Todo
- [x] add `Area` resource
- [x] add `measurements` resource (pageable)
- [x] make all `repositories` private final in controller
- [x] add DTOs for `measurements` as return type
- [x] look at [TestContainers](https://www.testcontainers.org) for JDBC Testing. @MicronautTest and @Testcontainers dont work together, fixed in Micronaut 2.0 ![response](MicronautTest_TestContainers.png)
- [x] use `@PastOrPresent` as validator on datetime
- [x] add `utilization` resource (with forecast)
- [ ] `HATEOAS` conform
- [x] OpenAPI documentation
- [x] look at Flyway how to configure/use in micronaut
- [ ] make forecasts with MadLib
- [x] test with GraalVM native-image - JDBC `@TypeHint(org.postgresql.Driver.class)`)
- [ ] declarative client needs custom errorType (decoder) for ConstraintViolationException
- [x] add ASE/WLAN Sensor Fetcher
- [ ] `Connection reset by peer` caused by netty server, see issue [micronaut-issue](https://github.com/micronaut-projects/micronaut-core/issues/3244)
- [x] use newest micronaut version 1.3.6