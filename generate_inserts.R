library(RPostgreSQL)
library(DBI)
library(dplyr)
library(tidyr)
library(xts)

options(stringsAsFactors = FALSE)

con <- dbConnect(RPostgres::Postgres(), 
                 user="aleksandarspasojevic",
                 password="220187Alex",
                 host="localhost",
                 port=5432,
                 dbname="seatfinder")
dbListTables(con)

camera <- dbGetQuery(con, "SELECT * FROM camera")
ase <- dbGetQuery(con, "SELECT * FROM ase")
wlan <- dbGetQuery(con, "SELECT * FROM wlan")

generate_seasonal_data <- function(fromto = "202004/202005/M"){
  semester <- Vectorize(
    function(month){
      if(month == 1){
        return("Prüfungswochen")
      } else if(month >= 2 & month <= 5){
        return("FS")
      } else if(month == 6){
        return("Prüfungswochen")
      } else if(month >= 7 & month <=8){
        return("Semesterunterbruch")
      } else if(month >= 9 & month <= 12){
        return("HS")
      }
    })
  
  mo_fr <- 1:5
  sa <- 6
  so <- 0
  opening_hours_mo_fr <- 8:19
  opening_hours_sa <- 9:15
  
  library_open <- Vectorize(
    function(weekday, hour){
      if(weekday == so){
        return(FALSE)
      } else if(weekday %in% mo_fr){
        if(hour %in% opening_hours_mo_fr){
          return(TRUE)
        }
        return(FALSE)
      } else if(weekday == sa){
        if(hour %in% opening_hours_sa){
          return(TRUE)
        }
        return(FALSE)
      }
    })
  
  opening <- Vectorize(
    function(open, weekday){
      if(!open)
        return(NA)
      if(weekday %in% mo_fr)
        return(min(opening_hours_mo_fr))
      if(weekday == sa)
        return(min(opening_hours_sa))
    })
  
  closing <- Vectorize(
    function(open, weekday){
      if(!open)
        return(NA)
      if(weekday %in% mo_fr)
        return(max(opening_hours_mo_fr))
      if(weekday == sa)
        return(max(opening_hours_sa))
    })
  
  timeslots <- data.frame(
    time = timeBasedSeq(fromto, retclass = 'POSIXlt')
  )
  
  grouped <- timeslots %>% mutate(
    month = as.integer(format(time, "%m")),
    weekday = as.integer(format(time, "%w")),
    hour = as.integer(format(time, "%H")),
    min = as.integer(format(time, "%M")),
    sec = as.integer(format(time, "%S"))) %>% 
    mutate(semester = semester(month)) %>% 
    mutate(library_open = library_open(weekday, hour)) %>% 
    mutate(opening = opening(library_open, weekday),
           closing = closing(library_open, weekday))
  
  return(grouped)
}

random <- Vectorize(
  function(semester, 
           weekday, 
           hour, 
           min, 
           sec, 
           library_open, 
           opening, 
           closing, 
           max_){
    if(library_open){
      if(semester == "Semesterunterbruch"){
        fraction <- hour + min/60 + sec/3600 
        random <- round(sin(pi*(fraction-opening)/(closing-opening)) * (max_/2)) + sample(-2:2, 1)
        if(random > max_)
          random <- max_
        if(random < 0)
          random <- 0
        return(random)
      } else if(semester == "Prüfungswochen"){
        return(sample((max_-4):max_,1))
      } else if(semester %in% c("FS", "HS")){
        fraction <- hour + min/60 + sec/3600
        random <- round(sin(pi*(fraction-opening)/(closing-opening)) * max_) + sample(-2:2, 1)
        if(random > max_)
          random <- max_
        if(random < 0)
          random <- 0
        return(random)
      }
    } else {
      return(0)
    }
  }
)

#----Count Table----
get_count_table <- function(sensor_id, table_count=8){
  data <- generate_seasonal_data() %>% 
    mutate(table_occupied = random(semester, weekday, hour, min, sec, 
                                   library_open, opening, closing, 
                                   max_ = table_count)) %>% 
    mutate(table_free = table_count-table_occupied) %>% 
    mutate(sensor_id = sensor_id) %>% 
    select(sensor_id,
           time,
           table_occupied,
           table_free)
  
  return(data)
}

do.call(rbind, lapply(camera$id, get_count_table)) %>% 
  arrange(time) %>% 
  dbAppendTable(con, "count_table", .)



#----Delta People----
delta <- function(count_people){
  person_in <- c(0, diff(count_people))
  return(person_in)
}

get_delta_people <- function(sensor_id, max_people=380){
  data <- generate_seasonal_data() %>% 
    mutate(count_person = random(semester, weekday, hour, min, sec, 
                                 library_open, opening, closing, 
                                 max_ = max_people)) %>% 
    group_by(date = format(time, "%x")) %>% 
    mutate(delta = delta(count_person),
           person_in = ifelse(delta < 0, 0, delta),
           person_out = ifelse(delta < 0, -delta, 0)) %>% 
    ungroup() %>% 
    mutate(sensor_id = sensor_id) %>% 
    select(sensor_id,
           time,
           person_in,
           person_out)
  
  return(data)
}

do.call(rbind, lapply(ase$id, get_delta_people)) %>% 
  arrange(time) %>% 
  dbAppendTable(con, "delta_people", .)


#----Count Clients----
get_count_client <- function(sensor_id, max_people=160){
  data <- generate_seasonal_data() %>% 
    mutate(count_clients = random(semester, weekday, hour, min, sec,
                                  library_open, opening, closing, 
                                  max_ = max_people)) %>% 
    mutate(sensor_id = sensor_id) %>% 
    select(sensor_id,
           time,
           count_clients)
  
  return(data)
}

do.call(rbind, lapply(wlan$id, get_count_client)) %>%
  arrange(time) %>% 
  dbAppendTable(con, "count_client", .)






#while (TRUE) {
#sapply(camera$id, insert_count_table)
#sapply(ase$id, insert_delta_people)
#sapply(wlan$id, insert_count_session)
#Sys.sleep(1)
#}
